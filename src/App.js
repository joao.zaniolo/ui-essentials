import TodoList from './components/TodoList';

import 'tailwindcss/tailwind.css';

export default function App() {
  return <TodoList />;
}
