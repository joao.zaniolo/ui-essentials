import { FiTrash } from 'react-icons/fi';

export default function Todo({ todo, onChange, onDelete }) {
  console.log(todo);
  return (
    <div className="flex items-center p-2 border rounded space-x-2">
      <label htmlFor="done" className="flex items-center flex-1 space-x-2">
        <input
          type="checkbox"
          name="done"
          id="done"
          checked={todo.isDone}
          onChange={onChange}
        />
        <span className={`${todo.isDone ? 'line-through text-gray-500' : ''}`}>
          {todo.text}
        </span>
      </label>
      <button
        type="button"
        onClick={onDelete}
        title="Delete todo"
        className="rounded-full w-7 h-7 grid place-items-center transition-all hover:bg-red-200 hover:text-red-500"
      >
        <FiTrash size={14} />
      </button>
    </div>
  );
}
