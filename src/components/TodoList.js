import { useEffect, useState, useRef } from 'react';

import Todo from './Todo';
import api from '../services/axios';

export default function TodoList() {
  const newTodoRef = useRef(null);
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    async function loadTodos() {
      const response = await api.get('/todos');

      setTodos(response.data);
    }

    loadTodos();
  }, []);

  async function createTodo(event) {
    event.preventDefault();

    const response = await api.post('/todos', {
      text: newTodoRef.current?.value,
      isDone: false,
    });

    newTodoRef.current.value = '';
    setTodos([response.data, ...todos]);
  }

  async function toggleTodo({ id, isDone }) {
    setTodos(
      todos.map(todo => {
        return todo.id === id ? { ...todo, isDone: !isDone } : todo;
      }),
    );

    await api.patch(`/todos/${id}`, {
      isDone: !isDone,
    });
  }

  async function deleteTodo(id) {
    setTodos(todos.filter(todo => todo.id !== id));
    await api.delete(`/todos/${id}`);
  }

  return (
    <div className="max-w-sm mx-auto bg-gray-100 rounded shadow-lg m-4 p-4 space-y-4">
      <h1 className="text-2xl font-medium text-blue-600">TODO APP</h1>

      <form onSubmit={createTodo}>
        <input
          type="text"
          ref={newTodoRef}
          placeholder="Walk the dog"
          className="p-2 w-full rounded focus:border-blue-500 placeholder-gray-400"
        />
      </form>

      {todos.length ? (
        todos.map(todo => {
          return (
            <Todo
              key={todo.id}
              todo={todo}
              onChange={() => toggleTodo(todo)}
              onDelete={() => deleteTodo(todo.id)}
            />
          );
        })
      ) : (
        <p className="w-full text-center italic text-gray-400">
          There are no todos...
        </p>
      )}
    </div>
  );
}
