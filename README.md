# TODO APP

An app to be evaluated as the UI essentials assignment

## Tech Stack

**Client:** React, TailwindCSS, Webpack, PostCSS, JSON Server

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/joao.zaniolo/ui-essentials
```

Go to the project directory

```bash
  cd ui-essentials
```

Install dependencies

```bash
  yarn install
```

Start the server

```bash
  yarn dev
```

## Authors

- [@jvzaniolo](https://www.github.com/jvzaniolo)
